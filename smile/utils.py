#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Дополнительные утилиты для работы программы.
"""


# Стандартные модули
import sys
import argparse
import logging
import pathlib
import os
import subprocess

# Пользовательские модули и переменные
# Расположение AStyle
ASTYLE_WIN_32 = pathlib.Path(__file__).parent / pathlib.Path("resources/astyle_win32.exe")
ASTYLE_LINUX_32 = pathlib.Path(__file__).parent / pathlib.Path("resources/astyle_linux32")
ASTYLE_LINUX_64 = pathlib.Path(__file__).parent / pathlib.Path("resources/astyle_linux64")

# Сторонние модули

# Включение логов
log = logging.getLogger(__name__)


# Исключения
# ----------------------------------------------------------------------------------------------------------------------
class Error(Exception):
    """
    Основной класс исключений.
    """
    pass


class APKError(Error):
    """
    Исключения, возникающие при работе с АПК.
    """

    def __init__(self, message, apk_name, apk_path=None):
        """
        :param message: Сообщение ошибки.
        :type message: str
        :param apk_name: Наименование АПК, в котором произошла ошибка.
        :type apk_name: str
        :param apk_path: Путь АПК.
        :type apk_path: str
        """
        self.message = message
        self.apk_name = apk_name
        self.apk_path = apk_path if apk_path else None

    def __str__(self):
        return ("АПК: {}; {}; Путь: {}".format(self.apk_name, self.message, self.apk_path)
                if self.apk_path else
                "АПК: {}; {}".format(self.apk_name, self.message))


class SPOError(Error):
    """
    Исключения, возникающие при работе с СПО.
    """

    def __init__(self, message, spo_name, parent_apk, path=None):
        self.message = message
        self.spo_name = spo_name
        self.parent_apk = parent_apk
        self.path = path if path else None

    def __str__(self):
        return ("АПК: {}, СПО: {}; {}; Путь: {}".format(self.parent_apk, self.spo_name, self.message, self.path)
                if self.path else
                "АПК: {}, СПО: {}; {}".format(self.parent_apk, self.spo_name, self.message))


class DatabaseConfigError(Error):
    """
    Исключения, возникающие при работе с БД конфигурации.
    """
    pass


class UtilsError(Error):
    """
    Исключения, возникающие при работе дополнительных инструментов.
    """

    def __init__(self, message, utility):
        self.message = message
        self.utility = utility

    def __str__(self):
        return "{}: {}".format(self.utility, self.message)


class MenuError(Error):
    """
    Исключения, возникающие при работе в интерактивном режиме.
    """
    pass


class MenuInputAbort(Error):
    """
    Исключения, возникающие при прерывании ввода пользователем.
    """
    pass


class MenuYesAnswer(Error):
    """
    Исключения, возникающие при согласии пользователя.
    """
    pass


class PrepareError(Error):
    """
    Исключения, возникающие при подготовке проекта и файлов к анализу.
    """
    pass


class StaticError(Error):
    """
    Исключения, возникающие при статическом анализе проекта.
    """
    pass


class MarkerError(Error):
    """
    Исключения, возникающие при работе с маркерами.
    """
    pass


# Суперклассы
# ----------------------------------------------------------------------------------------------------------------------
class OverLoadStr:
    """
    Класс перегружает __str__ метод.
    """

    def __str__(self):
        return self._attr_get()

    def _attr_get(self):
        res = "{} at <{}>\n".format(self.__class__, hex(id(self)))
        for attr in sorted(self.__dict__):
            res += "\t{}: {}\n".format(attr, repr(getattr(self, attr)))
        return res


# Утилиты
# ----------------------------------------------------------------------------------------------------------------------
class AStyle:
    """
    Класс утилиты AStyle.
    """

    def __init__(self, path, options, astyle_path=None):
        """
        :param path: Путь до файла для форматирования.
        :type path: str
        :param options: Опции форматирования.
        :type options: tuple
        :param astyle_path: Путь до исполняемого файла AStyle
        :type astyle_path: str
        """
        log.debug("Инициализация AStyle.")
        self.path = pathlib.Path(path)
        self.options = options
        self.astyle_path = pathlib.Path(astyle_path) if astyle_path else ([ASTYLE_LINUX_32, ASTYLE_WIN_32]
                                                                          [os.name == "nt"])
        log.debug("path: %s, options: %s, astyle_path: %s", self.path, self.options, self.astyle_path)

    def format(self):
        """
        Функция форматирования.
        """
        command_line = [str(self.astyle_path)]
        command_line += self.options
        # Заменять файл
        command_line.append("--suffix=none")
        command_line.append(str(self.path))
        log.debug(command_line)
        if subprocess.call(command_line, stdout=subprocess.DEVNULL):
            raise UtilsError("Ошибка форматирования файла {}, с опциями {}, astyle_path: {}".
                             format(self.path, self.options, self.astyle_path), "AStyle")


def argument_parser(func):
    def inner():
        parser = argparse.ArgumentParser(description="Дополнительный инструментарий для ПО Улыбка.",
                                         add_help=False)

        # Русскоязычный help
        parser.add_argument("--help", "-h", action="help",
                            help="Показывает это сообщение и выходит")

        subparsers = parser.add_subparsers(title="Утилиты",
                                           help="Для получения справки запустите: %(prog)s 'утилита' -h",
                                           dest="subparser_name")

        # Парсер для astyle
        parser_astyle = subparsers.add_parser("astyle", help="Работает с ПО AStyle",
                                              add_help=False)
        parser_astyle.add_argument("--help", "-h", action="help",
                                   help="Показывает это сообщение и выходит")
        parser_astyle.add_argument("--options", "-o", nargs=argparse.REMAINDER,
                                   default=[""],
                                   help="Дополнительные опции (см. документацию AStyle)")
        parser_astyle.add_argument("file", metavar="ФАЙЛ/ДИРЕКТОРИЯ",
                                   help="Целевой файл или директория для форматирования")

        # Парсер для Understand
        parser_understand = subparsers.add_parser('understand', help="Работает с ПО Understand",
                                                  add_help=False)
        parser_understand.add_argument("--help", "-h", action="help",
                                       help="Показывает это сообщение и выходит")
        parser_understand.add_argument('--create', '-C', action="store_true", required=True,
                                       help="Создать базу Understand")
        args = parser.parse_args()

        # Если нет аргументов, то печатаем help
        if len(sys.argv) < 2:
            parser.print_help()
            parser.exit()

        return func(args)

    return inner


@argument_parser
def run_utility(parser):
    if parser.subparser_name == "astyle":
        AStyle(parser.file, parser.options).format()


if __name__ == '__main__':
    try:
        import logger

        logger.setup_logging()
        log = logging.getLogger(__name__)
    except ImportError:
        logging.basicConfig(level="DEBUG")

    sys.exit(run_utility())
