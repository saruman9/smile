#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Модуль для подготовки файлов, проектов и т.д. для анализа.
"""

# Стандартные модули
import logging
import pathlib
import shutil

# Пользовательские модули и переменные
from smile.utils import OverLoadStr
from smile.utils import PrepareError
from smile.utils import AStyle
from smile.project import SPO
from smile import LANGUAGES

# Сторонние модули
import chardet

# Включение логов
log = logging.getLogger(__name__)


# TODO открывать файлы с аргументом - errors=surrogateescape
# Это требуется для обработки файлов с неизвестной кодировкой

class Prepare(OverLoadStr):
    """
    Класс подготовки исходников, проекта, файлов и т.д. для анализа.
    """

    def __init__(self, spo_object):
        """
        Инициализация класса Prepare.
        :param spo_object: Объект СПО
        :type spo_object: SPO
        """
        self.spo_object = spo_object

        self.source_path = pathlib.Path(self.spo_object.source_path)
        self.lab_path = pathlib.Path(self.spo_object.lab_path)
        self.languages = self.spo_object.languages
        # jar - архив, поэтому его не добавляем в список для реформатирования
        self.extensions_format = [ext for key in LANGUAGES for ext in LANGUAGES[key]["EXT"]
                                  if LANGUAGES[key]["ASTYLE_OPT"]]
        self.extensions = [ext for key in LANGUAGES for ext in LANGUAGES[key]["EXT"]]

    # TODO Позже обратить внимание на вставку маркеров, понадобится реализовать перенос пустых скобок{}?
    # TODO Позже задать правильные опции реформатирования для всех языков
    def reformat_astyle_src(self):
        """
        Реформатирует все файлы в папке с исходными данными, расширение которых содержится в EXT_LANG
        :return:
        """
        log.debug("Реформатирование исходных данных")
        self._check_source()
        language = ""
        files = self.walk_in_dir(self.source_path)
        for file in files:
            if file.suffix.lower() in self.extensions_format:
                for lang in LANGUAGES:
                    if file.suffix.lower() in LANGUAGES[lang]["EXT"]:
                        language = lang
                        break
                log.debug("Форматирование файла {}".format(file))
                log.debug("Опции форматирования: {}".format(LANGUAGES[language]["ASTYLE_OPT"]))
                if LANGUAGES[language]["ASTYLE_OPT"]:
                    AStyle(file, options=LANGUAGES[language]["ASTYLE_OPT"]).format()
                else:
                    log.debug("Файл {} не был форматирован".format(file))

    def src_to_utf8(self):
        """
        Перекодирует исходные данные в UTF-8.
        :return:
        """
        log.debug("Перекодирование исходных данных")
        self._check_source()
        files = self.walk_in_dir(self.source_path)
        for file in files:
            if file.suffix.lower() in self.extensions_format:
                self.file_to_utf8(file)

    @staticmethod
    def file_to_utf8_with_chardet(path):
        """
        Меняет кодировку файла на UTF-8.
        Использует chardet для определения кодировки. Если вероятность меньше 90,
        то пытаемся по списку.
        При невозможности пытается перекодировать (по порядку) из UTF-8, CP1251, KOI8-R, CP866, ISO8859-5, MAC_CYRILLIC.
        :param path: Файл для перекодировки
        :type path: pathlib.Path
        :return: Путь до вновь созданного файла
        """
        log.debug("Смена кодировки в файле на UTF-8")

        if not path.is_file():
            raise PrepareError("Ожидался файл для перекодировки [{}]".format(path))

        encodings = ["utf8", "cp1251", "koi8_r", "cp866", "iso8859_5", "mac_cyrillic"]
        success = False
        contains = ""
        size_of_file = path.stat().st_size

        if size_of_file > 5 * 2 ** 10 * 2 ** 10:
            log.warning("Размер файла больше 5 MiB({} MiB) [{}]".format(size_of_file / 2 ** 10 / 2 ** 10, path))
        if not size_of_file:
            log.debug("Файл пустой")
            success = True
        else:
            # Считываем бинарный файл
            with open(str(path), mode='rb') as file:
                contains_byte = file.read()

            # chardet определяет кодировку
            try:
                import chardet
            except ImportError:
                log.error("chardet не установлен!")
                Prepare.file_to_utf8(path)
                return

            encoding_chardet = chardet.detect(contains_byte)
            log.debug("chardet: {}".format(encoding_chardet))
            # Если confidence (вероятность) < 90%, то пытаемся своими силами
            if encoding_chardet["confidence"] < 0.9:
                for encoding in encodings:
                    try:
                        contains = contains_byte.decode(encoding=encoding)
                    except UnicodeDecodeError:
                        log.error("{} - ошибка для [{}]".format(encoding, path))
                        continue
                    else:
                        log.debug("{} - успешно для [{}]".format(encoding, path))
                        success = True
                        break
            else:
                try:
                    contains = contains_byte.decode(encoding=encoding_chardet["encoding"])
                except UnicodeDecodeError:
                    log.error("chardet определил неверно для [{}]".format(path))
                else:
                    log.debug("Успешно chardet")
                    success = True

        if not success:
            raise PrepareError("Не смог определить кодировку файла [{}]".format(path))
        else:
            log.debug("Запись файла с новой кодировкой")
            with open(str(path), 'w+', encoding="utf8") as file:
                file.write(contains)

    # TODO Позже обращать внимание на все попытки обращения к перекодированному файлу!
    @staticmethod
    def file_to_utf8(path):
        """
        Меняет кодировку файла на UTF-8.
        По порядку из UTF-8, CP1251, KOI8-R, CP866, ISO8859-5, MAC_CYRILLIC.
        :param path: Файл для перекодировки
        :type path: pathlib.Path
        :return: Путь до вновь созданного файла
        """
        log.debug("Смена кодировки в файле на UTF-8 без chardet")

        if not path.is_file():
            raise PrepareError("Ожидался файл для перекодировки [{}]".format(path))

        encodings = ["utf8", "cp1251", "koi8_r", "cp866", "iso8859_5", "mac_cyrillic"]
        success = False
        contains = ""
        size_of_file = path.stat().st_size

        if size_of_file > 5 * 2 ** 10 * 2 ** 10:
            log.warning("Размер файла больше 5 MiB({} MiB) [{}]".format(size_of_file / 2 ** 10 / 2 ** 10, path))
        if not size_of_file:
            log.debug("Файл пустой")
            success = True
        else:
            # Считываем бинарный файл
            with open(str(path), mode='rb') as file:
                contains_byte = file.read()

            for encoding in encodings:
                try:
                    contains = contains_byte.decode(encoding=encoding)
                except UnicodeDecodeError:
                    log.warning("{} - ошибка для [{}]".format(encoding, path))
                    encoding = chardet.detect(contains_byte)
                    try:
                        contains = contains_byte.decode(encoding=encoding["encoding"])
                    except UnicodeDecodeError:
                        log.warning("{} - ошибка для [{}]".format(encoding, path))
                        continue
                    else:
                        log.debug("{} - успешно для [{}]".format(encoding, path))
                        success = True
                        break
                else:
                    log.debug("{} - успешно для [{}]".format(encoding, path))
                    success = True
                    break
        if not success:
            raise PrepareError("Не смог определить кодировку файла [{}]".format(path))
        else:
            log.debug("Запись файла с новой кодировкой")
            with open(str(path), 'w+', encoding="utf8") as file:
                file.write(contains)

    def copy_source_to_lab(self):
        """
        Копирует исходные данные в лабораторные.
        """
        log.debug("Копирование исходных данных в лабораторные")
        self._check_source()
        self._check_lab()
        self.delete_lab()

        self._copy_src_to_dst(self.source_path, self.lab_path)

    def delete_lab(self, save_dir=False):
        """
        Удаляет папку с лабораторными данными
        """
        log.debug("Удаление папки с лабораторными файлами")

        self._check_lab()
        shutil.rmtree(str(self.lab_path))

        # После рекурсивного удаления следует создать папку с лабораторными данными
        if save_dir:
            self.lab_path.mkdir()

    def search_languages(self):
        """
        Поиск языков программирования, исходя из расширения файла.
        :return: Словарь. Язык программирования ассоциирован со списком файлов.
                    {"C": [file1, file2, ...], "C++": [file1, ...], ...}
        """
        self._check_source()
        files = self.walk_in_dir(self.source_path)

        # Формируем словарь языков вида {"C": [file1, file2, ...], "C++": [file1, ...]}
        files_languages = dict()
        for file in files:
            for language in LANGUAGES:
                if file.suffix.lower() in LANGUAGES[language]["EXT"]:
                    log.debug("Файл [{}] является исходником ЯП {}".format(file, language))
                    try:
                        files_languages[language].append(file)
                    except KeyError:
                        files_languages[language] = [file]

        return files_languages

    def get_size_of_source(self):
        """
        Размер исходных данных по языкам и в общем.
        :return: Словарь вида {"C#": [size_of_C#_files(int), size_of_C#_files{B, KiB, MiB, GiB}(str),
                               "C++": [size_of_c++_files(int), size_of_c++_files{B, KiB, MiB, GiB}(str)],
                                ...}
        """
        self._check_source()
        files_languages = self.search_languages()
        size_of_source_files = dict()
        size_of_source_files["ALL"] = [0, "0 B"]

        # Из словаря языков формируем словарь вида {"C#": [size_of_C#_files(int), size_of_C#_files(str),
        #  "C++": [size_of_c++_files(int), size_of_c++_files(str)], ...}
        for language in files_languages:
            for file in files_languages[language]:
                try:
                    size_of_source_files[language][0] += file.stat().st_size
                except (TypeError, KeyError):
                    size_of_source_files[language] = [0, "0 B"]
                    size_of_source_files[language][0] += file.stat().st_size
            log.debug(size_of_source_files)

            if size_of_source_files[language][0] > 2 ** 30:
                size_of_source_files[language][1] = "{:.2f} GiB".format(size_of_source_files[language][0] / (2 ** 30))
            elif size_of_source_files[language][0] > 2 ** 20:
                size_of_source_files[language][1] = "{:.2f} MiB".format(size_of_source_files[language][0] / (2 ** 20))
            elif size_of_source_files[language][0] > 2 ** 10:
                size_of_source_files[language][1] = "{:.2f} KiB".format(size_of_source_files[language][0] / (2 ** 10))
            else:
                size_of_source_files[language][1] = "{} B".format(size_of_source_files[language][0])

        size_of_source_files["ALL"][0] = sum([size[0] for size in size_of_source_files.values()])
        if size_of_source_files["ALL"][0] > 2 ** 30:
            size_of_source_files["ALL"][1] = "{:.2f} GiB".format(size_of_source_files["ALL"][0] / (2 ** 30))
        elif size_of_source_files["ALL"][0] > 2 ** 20:
            size_of_source_files["ALL"][1] = "{:.2f} MiB".format(size_of_source_files["ALL"][0] / (2 ** 20))
        elif size_of_source_files["ALL"][0] > 2 ** 10:
            size_of_source_files["ALL"][1] = "{:.2f} KiB".format(size_of_source_files["ALL"][0] / (2 ** 10))
        else:
            size_of_source_files["ALL"][1] = "{} B".format(size_of_source_files["ALL"][0])

        return size_of_source_files

    def _copy_src_to_dst(self, source, destination):
        """
        Метод для копирования содержимого папки source в папку destination.
        Папка destination создаётся во время копирования, следовательно её не должно существовать изначально.
        :param source: Папка, из которой происходит копирование
        :type source: pathlib.Path
        :param destination: Папка, в которую происходит копирование
        :type destination: pathlib.Path
        """
        log.debug("Копирование папки {} в папку {}".format(source, destination))

        if not source.exists():
            raise PrepareError("Папка не существует [{}]".format(source))
        if destination.exists():
            raise PrepareError("Папка уже существует [{}]".format(destination))
        try:
            self.walk_in_dir(source).__next__()
        except StopIteration:
            raise PrepareError("Папка пуста [{}]".format(source))

        try:
            shutil.copytree(str(source), str(destination))
        except shutil.Error as err:
            raise PrepareError(err)

    def _check_source(self):
        """
        Метод для проверки папки исходных данных на наличие файлов в ней.
        """
        log.debug("Проверка исходных данных")

        if not self.source_path.exists():
            raise PrepareError("Папки с исходными данными не существует [{}]".format(self.source_path))
        source_files = self.walk_in_dir(self.source_path)
        try:
            source_files.__next__()
        except StopIteration:
            raise PrepareError("Папка с исходными данными пуста [{}]".format(self.source_path))

    def _check_lab(self):
        """
        Метод для проверки папки лабораторных данных.
        """
        log.debug("Проверка лабораторных данных")

        if not self.lab_path.exists():
            raise PrepareError("Папки с лабораторными данными не существует [{}]".format(self.lab_path))

    # TODO Не переходит по ссылкам!
    @staticmethod
    def walk_in_dir(path):
        """
        Метод для рекурсивного сканирования папок.
        :param path: Путь для сканирования
        :type path: pathlib.Path
        :return: Список всех файлов (рекурсивно)
        :rtype: generator
        """
        log.debug("Сканирование папок, поиск файлов")

        if not path.exists():
            raise PrepareError("Папки не существует [{}]".format(path))

        for file in sorted(path.iterdir()):
            if file.is_dir():
                for file_in_dir in Prepare.walk_in_dir(file):
                    yield file_in_dir
            elif file.is_file():
                yield file


if __name__ == "__main__":
    pass
