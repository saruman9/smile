#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Модуль для проведения статического анализа СПО.
"""

# Стандартные модули
import logging
import pathlib

# Пользовательские модули и переменные
from smile.static import prepare, collectinfo
from smile.marker.marker import Marker
from smile.project import SPO
from smile.utils import MarkerError, OverLoadStr

# Сторонние модули

# Включение логов
log = logging.getLogger(__name__)


# TODO AStyle - не будет использоваться!
# TODO Перекодирование - не будет использоваться!
# TODO Все данные будут браться из Understand!
class Static(OverLoadStr):
    """
    Класс для проведения статического анализа.
    """

    def __init__(self, spo_object):
        """
        Инициализация класса.
        :param spo_object: Объект СПО
        :type spo_object: SPO
        """
        log.debug("Начата инициализация объекта статического анализа")
        self.spo_object = spo_object
        self.prepare_object = prepare.Prepare(self.spo_object)
        self.collect_info_object = collectinfo.CollectInfo(self.spo_object)

        self.name = self.spo_object.name
        self.parent_apk = self.spo_object.parent_apk
        self.level = self.spo_object.level
        self.languages = self.spo_object.languages

        self.path = self.spo_object.path
        self.source_path = self.spo_object.source_path
        self.lab_path = self.spo_object.lab_path

        self.udb_path = self.spo_object.udb_path
        self.sdb_path = self.spo_object.sdb_path

        log.debug(self)

    def run(self):
        """
        Запускает полный статический анализ.
        """
        log.debug("Статический анализ")
        log.info("Проведение статического анализа")
        self.prepare_spo()
        self.insert_markers()

    def prepare_spo(self):
        """
        Подготовить СПО для анализа.
        """
        log.debug("Подготовка для статического анализа")
        # log.info("Реформатирование исходных данных AStyle'ом")
        # self.prepare_object.reformat_astyle_src()
        log.info("Копирование исходных данных в лабораторные")
        self.prepare_object.copy_source_to_lab()
        log.info("Заполнение базы данных")
        self.collect_info_object.save_db()

    def insert_markers(self):
        """
        Вставка маркеров в лабораторную часть.
        """
        log.debug("Вставка маркеров предназначенных для статического анализа")
        for row in self.spo_object.get_source_files_from_db():
            try:
                Marker(row.id, pathlib.Path(row.full_name), "static", language=row.language).insert()
            except MarkerError as err:
                log.warning(err)

    def check_markers(self):
        """
        Проверка маркеров, вставленных в лабораторную часть.
        """
        log.debug("Проверка вставки маркеров")
        for row in self.spo_object.get_source_files_from_db():
            log.debug("id: {}, full_name: {}, language: {}".format(row.id, row.full_name, row.language))
            try:
                if Marker(row.id, pathlib.Path(row.full_name), "static", language=row.language).check():
                    log.debug("Маркер вставлен в файл {}".format(row.full_name))
                else:
                    log.warning("Маркера нет в файле {}".format(row.full_name))
            except MarkerError as err:
                log.warning(err)
