#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Модуль для работы с SQLite базой данных
"""

# Стандартные модули
import pathlib
import logging

# Пользовательские модули
from smile import PATH_CONF

# Сторонние модули
from peewee import *


# Включить логирование
log = logging.getLogger(__name__)

# Инициализация БД во время выполнения
database = SqliteDatabase(None)
database_config = SqliteDatabase(None)


def init_database(path_db):
    """
    Initialize database from path_db for operate with it
    :param path_db: Absolute or relative path to SQLite database
    :type path_db: str
    """
    log.debug("Начата инициализация основной БД.")
    path_db = pathlib.Path(path_db)
    if path_db.exists():
        log.debug("БД существует.")
        database.init(str(path_db))
    else:
        log.debug("Создание БД.")
        path_db.touch()
        database.init(str(path_db))
        create_tables()


def init_database_config():
    """
    Initialize database for write config
    """
    log.debug("Начата инициализация БД конфигурации.")
    path_db = pathlib.Path(PATH_CONF + "/conf.db3")
    if path_db.exists():
        log.debug("БД конфигурации существует.")
        database_config.init(str(path_db))
        return True
    else:
        log.debug("Создание БД конфигурации.")
        path_db.touch()
        database_config.init(str(path_db))
        create_tables_config()
        return False


def create_tables():
    """
    Create tables Bin, Src, Function, Line, GVar, Pointer, BinSrcLink, FuncFuncLink, LineLine, GVarFuncLink
    :return: False(0) - Success, True(1) - Error
    :rtype: bool
    """
    log.debug("Начато создание таблиц в основной БД.")
    log.debug("Открывается соединение к БД.")
    database.connect()
    log.debug("Создание таблиц в БД.")
    database.create_tables(
        [Files, Binary, Source, Function, Line, GVar, Pointer, BinSrcLink, FuncFuncLink, LineLine, GVarFuncLink])
    log.debug("Таблицы успешно созданы.")
    log.debug("Закрывается соединение к БД.")
    database.close()


def create_tables_config():
    """
    Create tables APK, SPO
    :return: False(0) - Success, True(1) - Error
    :rtype: bool
    """
    log.debug("Начато создание таблиц БД конфигурации")
    log.debug("Открывается соединение к БД конфигурации.")
    database_config.connect()
    log.debug("Создание таблиц в БД конфигурации.")
    database_config.create_tables(
        [APK, SPO])
    log.debug("Таблицы БД конфигурации успешно созданы")
    log.debug("Закрывается соединение к БД.")
    database_config.close()


class BaseModel(Model):
    """
    Class for create table in SQLite database
    """

    class Meta:
        """
        Class for setting table
        """
        database = database


class ConfigModel(Model):
    """
    Class for create table in SQLite database
    """

    class Meta:
        """
        Class for setting table
        """
        database = database_config


class APK(ConfigModel):
    """
    Class of table APK in SQLite database
    """

    id = PrimaryKeyField()
    name = CharField()
    path = CharField()


class SPO(ConfigModel):
    """
    Class of table SPO in SQLite database
    """

    id = PrimaryKeyField()
    name = CharField()
    parent_apk = ForeignKeyField(APK, related_name="spo")
    path = CharField()
    source_path = CharField()
    lab_path = CharField()
    udb_path = CharField()
    sdb_path = CharField()
    level = IntegerField()
    languages = CharField()


class Function(BaseModel):
    """
    Class of table Function in SQLite database
    """

    id = PrimaryKeyField()
    name = CharField()
    ret_type = CharField()
    type_param = CharField()
    count_line = IntegerField()
    def_file = CharField()
    def_offset = IntegerField()
    dec_file = CharField()
    is_useful = BooleanField()
    udb_id = IntegerField()


class GVar(BaseModel):
    """
    Class of table gVar in SQLite database
    """

    id = PrimaryKeyField()
    name = CharField()
    type = CharField()
    def_file = CharField()
    def_offset = IntegerField()


class Files(BaseModel):
    """
    Класс таблицы Files в SQLite базе данных.
    Содержит информацию о всех файлах проекта.
    """

    full_name = CharField(unique=True,
                          help_text="Полное имя файла, включает в себя абсолютный путь к файлу")
    type = CharField(help_text="Тип файла (source, binary, resource)")
    checksum = CharField(help_text="Контрольная сумма файла")
    size = IntegerField(help_text="Размер файла в байтах")
    lines = IntegerField(null=True,
                         help_text="Количество строк в файле")
    ctime = DateTimeField(help_text="Время изменения метаданных (Unix), время создания (Windows)")


class Binary(BaseModel):
    """
    Класс таблицы Binary в SQLite базе данных.
    Содержит информацию о всех бинарных файлах проекта.
    """

    full_name = CharField(unique=True,
                          help_text="Полное имя файла, включает в себя абсолютный путь к файлу")
    checksum = CharField(help_text="Контрольная сумма файла")
    size = IntegerField(help_text="Размер файла в байтах")
    ctime = DateTimeField(help_text="Время изменения метаданных (Unix), время создания (Windows)")


class Source(BaseModel):
    """
    Класс таблицы Source в SQLite базе данных.
    Содержит информацию о всех исходных файлах (исходники) проекта.
    """

    full_name = CharField(unique=True,
                          help_text="Полное имя файла, включает в себя абсолютный путь к файлу")
    language = CharField(help_text="Язык программирования")
    checksum = CharField(help_text="Контрольная сумма файла")
    size = IntegerField(help_text="Размер файла в байтах")
    lines = IntegerField(null=True,
                         help_text="Количество строк в файле")
    ctime = DateTimeField(help_text="Время изменения метаданных (Unix), время создания (Windows)")
    is_used = BooleanField()
    is_marked = BooleanField()


class Line(BaseModel):
    """
    Class of table Line in SQLite database
    """

    id = PrimaryKeyField()
    func = ForeignKeyField(Function, related_name='line')
    id_line_in_func = IntegerField()
    begin = IntegerField()
    size = IntegerField()
    is_useful = BooleanField()


class Pointer(BaseModel):
    """
    Class of table Pointer in SQLite database
    """

    func_1 = ForeignKeyField(Function, related_name='pointer1')
    func_2 = ForeignKeyField(Function, related_name='pointer2')


class BinSrcLink(BaseModel):
    """
    Class of table BinSrcLink in SQLite database
    """

    bin = ForeignKeyField(Binary, related_name='src_links')
    src = ForeignKeyField(Source, related_name='bin_links')


class FuncFuncLink(BaseModel):
    """
    Class of table FuncFuncLink in SQLite database
    """

    parent = ForeignKeyField(Function, related_name='parent_func')
    child = ForeignKeyField(Function, related_name='child_func')


class LineLine(BaseModel):
    """
    Class of table LineLine in SQLite database
    """
    line_1 = ForeignKeyField(Line, related_name='line1')
    line_2 = ForeignKeyField(Line, related_name='line2')
    flag_return = BooleanField()
    func = ForeignKeyField(Function, related_name='line_line')


class GVarFuncLink(BaseModel):
    """
    Class of table GVarFuncLink in SQLite database
    """

    gvar = ForeignKeyField(GVar, related_name='func_links')
    func = ForeignKeyField(Function, related_name='gvar_links')
