#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Объекты типа АПК, СПО
"""

# Стандартные модули
import pathlib
import logging
import shutil
import itertools

# Пользовательские модули
from smile.utils import OverLoadStr
from smile import database
from smile.utils import (APKError, SPOError, DatabaseConfigError)
from smile import LANGUAGES


# Enable logging
log = logging.getLogger(__name__)


# TODO Дополнить методы
# TODO Сделать описание атрибута db
# TODO Добавить методы проверки и потом позже их вызывать для меню,
# TODO определять языки самостоятельно по расширению
#  а также внутри самих объектов при создании, удалении и т.д.?
class APK(OverLoadStr):
    """АПК Class
    Содержит в себе методы и свойства объекта типа АПК
    """

    def __init__(self, name=None, list_spo=None, path=None):
        """
        Инициализация АПК объекта
        :param name: Наименование АПК
        :type name: str
        :param list_spo: Список СПО в составе данного АПК
        :type list_spo: str
        :param path: Абсолютный путь до АПК
        :type path: str
        """
        log.debug("Инициализация объекта АПК.")
        if name:
            self.name = clear_string(name)
        if list_spo:
            self.list_spo = list_spo.split(',')
            self.list_spo = [clear_string(x) for x in self.list_spo if clear_string(x)]
        else:
            self.list_spo = list_spo
        # TODO Переконвертировать path в pathlib или в str?
        self.path = path
        self.db = database.APK()

    def create(self):
        """
        Создаёт АПК объект.
        """
        log.debug("Начато создание АПК.")

        # Существует ли данное АПК
        if self.exist():
            raise APKError("АПК уже имеется в БД, попробуйте его открыть", self.name)

        # Создание директории АПК
        self._create_path()

        # Сохранение в БД конфигурации
        self.save_db()

        log.debug("АПК успешно создано!")
        return self

    def _create_path(self):
        """
        Создание директории АПК.
        :return:
        """
        if self.path:
            # str путь в pathlib.Path
            self.path = pathlib.Path(self.path)
            if not self.path.exists():
                raise APKError("Директории АПК не существует", self.name, str(self.path))
        else:
            log.debug("Создание директории АПК.")
            self.path = pathlib.Path.cwd() / "Projects" / self.name
            try:
                self.path.mkdir(parents=True)
            except FileExistsError:
                log.warning("Директория или файл с таким названием уже существует. %s", self.path)

    def save_db(self):
        """
        Сохранить информацию об объекте в БД конфигурации.
        :return:
        """
        log.debug("Сохранение в БД конфигурации.")
        database.init_database_config()

        self.db.name = self.name
        self.db.path = str(self.path)
        self.db.save()

    def exist(self):
        """
        Проверка на существование АПК в БД конфигурации.
        :return:
        """
        log.debug("Проверка на существование АПК в БД конфигурации.")
        database.init_database_config()
        try:
            database.APK.get(database.APK.name == self.name)
        except database.DoesNotExist:
            return False
        else:
            return True

    def print(self):
        """
        Печатает содержимое АПК объекта в форматированном виде.
        """
        self.open()
        print("Наименование АПК: {}".format(self.name))
        print("Путь: {}".format(self.path))
        print("Список СПО в составе АПК: ")
        for spo in self.list_spo:
            print("\t- {}".format(spo))

    def open(self):
        """
        Открывает АПК (извлекает информацию из БД конфигурации).
        """
        log.debug("Начато открытие АПК %s", self.name)
        log.debug("Получение информации об АПК из БД конфигурации.")
        database.init_database_config()
        log.debug("Проверка на существование АПК %s", self.name)
        try:
            self.db = database.APK.get(database.APK.name == self.name)
        except database.DoesNotExist:
            raise APKError("Не найден АПК", self.name)
        self.path = self.db.path
        self.list_spo = [spo.name for spo in database.SPO.select().where(database.SPO.parent_apk == self.db)]

    def delete(self):
        """
        Удаляет объект АПК со всеми дочерними СПО, директориями и БД.
        """
        log.debug("Начато удаление АПК %s.", self.name)
        log.debug("Получение информации об АПК из БД конфигурации.")
        if self.name == "NONE_APK":
            log.info("В БД конфигурации нет АПК.")
            raise DatabaseConfigError("Таблица АПК пуста")
        database.init_database_config()
        log.debug("Проверка на существование АПК %s", self.name)
        try:
            self.db = database.APK.get(database.APK.name == self.name)
        except database.DoesNotExist:
            raise APKError("Не найден АПК", self.name)
        log.debug(self.db.spo)
        if self.db.spo.exists():
            log.debug("Удаление СПО из состава АПК.")
            for spo_child in self.db.spo:
                spo = SPO(spo_child.name, self.name)
                try:
                    spo.delete()
                except (SPOError, DatabaseConfigError) as err:
                    raise APKError("Не удалось удалить файлы и директории АПК [{}]".format(err), self.name)
        log.debug("Удаление директории %s АПК %s.", self.path, self.name)
        try:
            shutil.rmtree(self.db.path)
        except FileNotFoundError as err:
            raise APKError("Не удалось удалить файлы и директории АПК [{}]".format(err), self.name)

        log.debug("Удаление записи об АПК %s из БД конфигурации.", self.name)
        self.db.delete_instance()
        return 0

    # TODO В хронологическом порядке или алфавитном?
    @staticmethod
    def get_list_apk():
        """
        Вывод из БД конфигурации наименований всех АПК.
        :return: None - в БД конфигурации нет АПК
        """
        log.debug("Получение информации об АПК из БД конфигурации.")
        database.init_database_config()
        apk_list = database.APK.select()
        if not apk_list.exists():
            raise DatabaseConfigError("Таблица АПК пуста")
        apk_list_name = [apk_name.name for apk_name in apk_list]
        log.debug("Наименования АПК в БД: %s", apk_list_name)
        return apk_list_name

    @staticmethod
    def debug_print_all():
        """
        Функция отладки - вывод всех АПК с дочерними СПО из БД конфигурации
        :return:
        """
        log.debug("Отладка. Вывод всего содержимого БД конфигурации.")
        database.init_database_config()
        all_spo = [spo.name for spo in database.SPO.select()]
        log.debug("Наименования СПО в БД: %s", all_spo)
        try:
            apk_all = APK.get_list_apk()
        except DatabaseConfigError as err:
            log.warning(err)
            return

        for apk_name in apk_all:
            apk_temp = APK(apk_name)
            try:
                apk_temp.print()
                log.debug(apk_temp)
            except APKError as err:
                log.warning(err)
            if not apk_temp.list_spo:
                log.warning("В составе АПК %s СПО отсутствуют.", apk_temp.name)
            else:
                for spo_name in apk_temp.list_spo:
                    all_spo.remove(spo_name)
                    spo_temp = SPO(spo_name, apk_name)
                    try:
                        spo_temp.print()
                        log.debug(spo_temp)
                    except SPOError as err:
                        log.warning(err)
                    if not spo_temp.languages or not spo_temp.languages[0]:
                        log.warning("В составе СПО %s отсутствуют языки.", spo_temp.name)

        if all_spo:
            log.warning("Данные СПО не имеют родительского АПК: %s", all_spo)

    @staticmethod
    def debug_create_test():
        """
        Функция отладки - создание тестового АПК "Тест".
        Создание в нём СПО "Тест".
        А также копирование исходных данных из папки Test projects.
        """
        log.debug("Отладка. Создание тестового АПК.")

        APK("Тест").create()
        spo = SPO("Тест", "Тест").create()
        pathlib.Path(spo.source_path).rmdir()
        project_py_path = pathlib.Path(__file__)
        shutil.copytree(str(project_py_path.parent.parent / "Test projects"), str(spo.source_path))


class SPO(OverLoadStr):
    """СПО Class
    Содержит в себе методы и свойства объекта типа СПО
    """

    def __init__(self, name, parent_apk, level=None, languages=None, path=None,
                 udb_path=None, sdb_path=None, source_path=None, lab_path=None):
        """
        Класс инициализации
        :param name: Наименование СПО
        :type name: str
        :param parent_apk: Родительский АПК
        :type parent_apk: str
        :param path: Абсолютный путь до СПО
        :type path: str
        :param level: Уровень анализа
        :type level: str
        :param languages: Список языков программирования в составе СПО
        :type languages: str
        :param udb_path: Абсолютный путь до базы данных Understand
        :type udb_path: str
        :param sdb_path: Абсолютный путь до SQLite базы данных
        :type source_path: str
        :param source_path: Абсолютный путь до директории с исходными данными
        :type lab_path: str
        :param lab_path: Абсолютный путь до директории с лабораторными данными
        """
        log.debug("Инициализация объекта СПО.")
        self.name = clear_string(name)
        self.parent_apk = parent_apk
        self.path = path
        self.level = int(level) if level else 4
        if languages:
            log.debug("Поданный languages: %s", languages)
            # Очистка языков от мусора
            self.languages = [clear_string(lang).upper() for lang in languages if
                              clear_string(lang).upper() in LANGUAGES]
            log.debug("languages после очистки: %s", self.languages)
            # Удаление дубликатов
            self.languages = list(set(self.languages))
            if not self.languages:
                log.warning("Пустой список языков")
        else:
            self.languages = ""
        self.udb_path = udb_path
        self.sdb_path = sdb_path
        self.source_path = source_path
        self.lab_path = lab_path
        self.db = database.SPO()
        # TODO Удалить, оставить, использовать?
        # Временная переменная базы данных SQLite
        self.sqlite = sdb_path

    def create(self):
        """
        Создание СПО объекта.
        """
        log.debug("Начато создание СПО")
        # Проверка на существование СПО в БД конфигурации
        if self.exist():
            raise SPOError("СПО в составе АПК уже существует в БД, попробуйте его открыть",
                           self.name, self.parent_apk)

        log.debug("Проверка на существование родительского АПК в БД конфигурации.")
        if self.parent_apk == "NONE_APK":
            raise DatabaseConfigError("Таблица АПК пуста")
        if not database.APK.select().where(database.APK.name == self.parent_apk).exists():
            raise SPOError("Родительского АПК не существует", self.name, self.parent_apk)

        # Создание директории СПО
        self._create_path()

        # Создание Understand базы данных СПО
        self._create_understand_db()

        # Создание SQLite базы данных СПО
        self._create_sqlite_db()

        # Создание директории с исходными данными
        self._create_source_path()

        # Создание директории с лабораторными данными
        self._create_lab_path()

        # Сохранение в БД конфигурации
        self.save_db()

        log.debug("СПО успешно создано!")
        return self

    def _create_path(self):
        """
        Создание директории проекта.
        :return:
        """
        if self.path:
            # Конвертируем str в pathlib.Path
            self.sdb_path = pathlib.Path(self.sdb_path)
            self.path = pathlib.Path(self.path)
            if not self.path.exists():
                raise SPOError("Директории СПО не существует", self.name, self.parent_apk, self.path)
        else:
            self.path = pathlib.Path.cwd() / "Projects" / self.parent_apk / self.name
            log.debug("Создание директории (%s) СПО.", self.path)
            try:
                self.path.mkdir(parents=True)
            except FileExistsError:
                log.warning("Директория или файл с таким названием %s уже существует.", self.path)

    def _create_understand_db(self):
        """
        Создание Understand базы данных.
        :return:
        """
        if self.udb_path:
            self.udb_path = pathlib.Path(self.udb_path)
            # конвертируем str в pathlib.Path
            self.sdb_path = pathlib.Path(self.sdb_path)
            if not self.udb_path.exists():
                raise SPOError("Understand базы данных не существует", self.name, self.parent_apk, self.udb_path)
        else:
            self.udb_path = pathlib.Path.cwd() / "Projects" / self.parent_apk / self.name / "Databases"
            log.debug("Создание Understand базы данных %s.", self.udb_path)
            try:
                self.udb_path.mkdir(parents=True)
            except FileExistsError:
                log.warning("Директория или файл с таким названием %s уже существует.", self.udb_path)
            self.udb_path /= (self.name + ".udb")
            self.udb_path.touch()

    def _create_sqlite_db(self):
        """
        Создание SQLite базы данных.
        :return:
        """
        if self.sdb_path:
            # конвертируем str в pathlib.Path
            self.sdb_path = pathlib.Path(self.sdb_path)
            if not self.sdb_path.exists():
                raise SPOError("SQLite базы данных не существует", self.name, self.parent_apk, self.sdb_path)
        else:
            self.sdb_path = pathlib.Path.cwd() / "Projects" / self.parent_apk / self.name / "Databases"
            log.debug("Создание SQLite базы данных %s.", self.sdb_path)
            try:
                self.sdb_path.mkdir(parents=True)
            except FileExistsError:
                # TODO Сделать raise SPOError?
                pass
            self.sdb_path /= (self.name + ".db3")

    def _create_source_path(self):
        """
        Создание директории для исходных данных.
        :return:
        """
        if self.source_path:
            # конвертируем str в pathlib.Path
            self.source_path = pathlib.Path(self.source_path)
            if not self.source_path.exists():
                raise SPOError("Директории с исходными данными не существует",
                               self.name, self.parent_apk, self.source_path)
        else:
            self.source_path = pathlib.Path.cwd() / "Projects" / self.parent_apk / self.name / "Source"
            log.debug("Создание директории с исходными данными (%s).", self.source_path)
            try:
                self.source_path.mkdir(parents=True)
            except FileExistsError:
                log.warning("Директория или файл с таким названием %s уже существует.", self.source_path)

    def _create_lab_path(self):
        """
        Создание директории для лабораторных данных.
        :return:
        """
        if self.lab_path:
            # конвертируем str в pathlib.Path
            self.lab_path = pathlib.Path(self.lab_path)
            if not self.lab_path.exists():
                raise SPOError("Директории с лабораторными данными не существует",
                               self.name, self.parent_apk, self.lab_path)
        else:
            self.lab_path = pathlib.Path.cwd() / "Projects" / self.parent_apk / self.name / "Lab"
            log.debug("Создание директории с лабораторными данными (%s).", self.lab_path)
            try:
                self.lab_path.mkdir(parents=True)
            except FileExistsError:
                log.warning("Директория или файл с таким названием %s уже существует.", self.lab_path)

    def save_db(self):
        """
        Сохранить информацию об объекте в БД конфигурации.
        :return:
        """
        log.debug("Создание записи в БД конфигурации.")
        database.init_database_config()
        self.db.name = self.name
        self.db.parent_apk = database.APK.get(database.APK.name == self.parent_apk)
        self.db.path = str(self.path)
        self.db.level = self.level
        self.db.languages = ",".join(self.languages) if len(self.languages) > 1 else "".join(self.languages)
        self.db.udb_path = str(self.udb_path)
        self.db.sdb_path = str(self.sdb_path)
        self.db.source_path = str(self.source_path)
        self.db.lab_path = str(self.lab_path)
        self.db.save()

    def exist(self):
        """
        Проверка СПО на существование в БД конфигурации.
        :return:
        """
        log.debug("Проверка на существование СПО в БД конфигурации.")
        database.init_database_config()
        try:
            database.SPO.select().join(database.APK).where((database.APK.name == self.parent_apk) &
                                                           (database.SPO.name == self.name)).get()
        except database.DoesNotExist:
            return False
        else:
            return True

    def delete(self):
        """
        Удаление СПО со всеми директориями и базами данных.
        """
        log.debug("Начато удаление СПО %s.", self.name)
        log.debug("Получение информации об СПО из БД конфигурации.")
        database.init_database_config()

        try:
            spo_bd = database.SPO.select().join(database.APK).where((database.APK.name == self.parent_apk) &
                                                                    (database.SPO.name == self.name)).get()
        except database.DoesNotExist:
            raise SPOError("СПО не найдено в БД конфигурации", self.name, self.parent_apk)

        self.path = spo_bd.path
        self.source_path = spo_bd.source_path
        self.udb_path = pathlib.Path(spo_bd.udb_path)
        self.sdb_path = pathlib.Path(spo_bd.sdb_path)
        log.debug("path: %s, source_path: %s, udb_path: %s, sdb_path: %s",
                  self.path, self.source_path, self.udb_path, self.sdb_path)

        log.debug("Начато удаление рабочих директорий СПО и БД.")
        try:
            self.udb_path.unlink()
            self.sdb_path.unlink()
            shutil.rmtree(self.source_path)
            shutil.rmtree(self.path)
        except FileNotFoundError as err:
            raise SPOError("Не удалось удалить файлы и директории СПО [{}]".format(err), self.name, self.parent_apk)

        log.debug("Удаление СПО из БД конфигурации.")
        spo_bd.delete_instance()

        return 0

    def open(self):
        """
        Открывает СПО (извлекает информацию из БД конфигурации).
        """
        log.debug("Начато открытие СПО %s, родительский АПК %s.", self.name, self.parent_apk)
        log.debug("Получение информации об СПО из БД конфигурации.")
        database.init_database_config()
        log.debug("Проверка на существование СПО %s, родительский АПК %s", self.name, self.parent_apk)
        try:
            self.db = database.SPO.select().join(
                database.APK).where(
                (database.SPO.name == self.name) &
                (database.APK.name == self.parent_apk)).get()
        except database.DoesNotExist:
            raise SPOError("Не найдено СПО в БД конфигурации", self.name, self.parent_apk)
        self.path = self.db.path
        self.source_path = self.db.source_path
        self.lab_path = self.db.lab_path
        self.udb_path = self.db.udb_path
        self.sdb_path = self.db.sdb_path
        self.level = self.db.level
        self.languages = self.db.languages.split(',') if self.db.languages else []

        return self

    def add_language(self, language):
        """
        Метод добавления языка программирования в СПО.
        :param language: Язык программирования (должен состоять в списке LANGUAGES)
        :type language: str
        :return:
        """
        log.debug("Добавление языка в СПО")
        self.open()
        if language.upper() not in LANGUAGES:
            raise SPOError("Язык {} не состоит в списке доступных языков".format(language), self.name, self.parent_apk)
        if language.upper() in self.languages:
            raise SPOError("Язык {} уже присутствует в СПО".format(language), self.name, self.parent_apk)

        self.languages.append(language.upper())
        self.save_db()

    def delete_language(self, language):
        """
        Метод удаления языка программирования из СПО.
        :param language: Язык программирования (должен состоять в списке LANGUAGES)
        :type language: str
        :return:
        """
        log.debug("Удаление языка из СПО")
        self.open()
        if language.upper() not in LANGUAGES:
            raise SPOError("Язык {} не состоит в списке доступных языков".format(language), self.name, self.parent_apk)
        if language.upper() not in self.languages:
            raise SPOError("Языка {} в СПО не существует".format(language), self.name, self.parent_apk)

        self.languages.remove(language.upper())
        self.save_db()

    def get_source_files_from_db(self):
        """
        Получает список исходных файлов из БД
        :return: Объект БД - SPO
        :rtype: database.SelectQuery
        """
        log.debug("Получение информации из БД о {}:{}".format(self.name, self.parent_apk))
        self.open()
        db = database.SqliteDatabase(self.sdb_path)
        with database.Using(db, [database.Source]):
            if database.Source.select().exists():
                return database.Source.select()
            else:
                raise SPOError("Таблица Source пуста", parent_apk=self.parent_apk, spo_name=self.name)

    def print(self):
        """
        Печатает содержимое СПО объекта в форматированном виде.
        :return: 1 - ошибка, 0 - успешно
        """
        # TODO exception
        self.open()
        print("СПО {}".format(self.name))
        print("Родительский АПК: {}".format(self.parent_apk))
        print("Путь: {}".format(self.path))
        print("Уровень контроля: {}".format(self.level))
        print("Путь до Understand базы данных: {}".format(self.udb_path))
        print("Путь до SQLite базы данных: {}".format(self.sdb_path))
        print("Путь до исходных данных: {}".format(self.source_path))
        print("Путь до лабораторных данных: {}".format(self.lab_path))
        print("Список используемых языков: ")
        for lang in self.languages:
            print("\t- {}".format(lang))

        return 0

    # TODO В хронологическом порядке или алфавитном?
    @staticmethod
    def get_list_spo(parent_apk=None):
        """
        Вывод из БД конфигурации наименований всех СПО.
        :param parent_apk: Наименование родительского АПК (если отсутствует будут выведены все СПО)
        :type parent_apk: str
        :return: 1 - в БД конфигурации нет СПО, 2 - нет родительского АПК
        """
        log.debug("Получение информации об СПО из БД конфигурации.")
        list_spo = list()
        database.init_database_config()
        # TODO Сделать обработку исключения, если родительского АПК не существует
        if parent_apk:
            log.debug("Информация об СПО из родительского АПК (%s).", parent_apk)
            list_spo = database.SPO.select().join(database.APK).where(database.APK.name == parent_apk)
            list_spo = list(zip(list_spo, itertools.repeat(parent_apk, len(list_spo))))
        else:
            list_spo_db = database.SPO.select()
            for spo in list_spo_db:
                list_spo.append((spo.name, spo.parent_apk.name))
        log.debug("Наименования СПО в БД: %s", list_spo)
        return list_spo


# TODO Найти модуль для очистки строки или дописать костыль
def clear_string(string):
    """
    Очищает строку от таких символов, как <>/\"';: ~`@#№$%^&*
    :param string: Строка для очистки
    :type string: str
    :return: Строка очищенная
    :rtype: str
    """
    garbage = "<>|/\\\"';:?`&*,"
    string = string.strip()
    for char in garbage:
        string = string.replace(char, '')
    if string == '.' or string == '..':
        string = string.replace('.', '\\.')
    return string
